package com.kenfogel.binarytree.comparators;

import com.kenfogel.binarytree.data.Book;

import java.util.Comparator;

/**
 * Comparator that compares the title of the books using the natural
 * string comparison
 */
public class BookComparator implements Comparator<Book> {
    @Override
    public int compare(Book o1, Book o2) {
        return o1.getTitle().compareTo(o2.getTitle());
    }
}
