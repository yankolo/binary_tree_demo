package com.kenfogel.binarytree.gui;

import com.kenfogel.binarytree.comparators.BookComparator;
import com.kenfogel.binarytree.data.Book;
import com.kenfogel.binarytree.implementation.BinaryTree;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;

import java.util.Comparator;

public class BinaryTreeController {
    @FXML
    private Canvas canvas;

    private GraphicBinaryTree<Book> graphicBinaryTree;

    @FXML
    public void initialize() {
        this.graphicBinaryTree = generateTree();
        this.graphicBinaryTree.draw();
    }

    /**
     * Helper method to generate a GraphicBinaryTree
     */
    private GraphicBinaryTree<Book> generateTree() {
        Comparator<Book> comparator = new BookComparator();
        BinaryTree<Book> binaryTree = new BinaryTree<>(comparator);

        Book[] books = generateBooks();

        for (Book book : books) {
            binaryTree.insert(book);
        }

        return new GraphicBinaryTree<Book>(canvas, binaryTree);
    }

    /**
     * Helper method to generate the data that will be in the tree
     */
    private Book[] generateBooks() {
        Book[] books = new Book[21];

        books[0] = new Book("Hamlet");
        books[1] = new Book("1984");
        books[2] = new Book("Brave New World");
        books[3] = new Book("Lord of the Rings");
        books[4] = new Book("Great Gatsby");
        books[5] = new Book("Animal Farm");
        books[6] = new Book("Green Eggs and Ham");
        books[7] = new Book("Remains of the Day");
        books[8] = new Book("Maus");
        books[9] = new Book("Things Fall Apart");
        books[10] = new Book("Knights Of The Sea");
        books[11] = new Book("Aliens Of The Light");
        books[12] = new Book("Humans And Trees");
        books[13] = new Book("Gangsters And Slaves");
        books[14] = new Book("Misfortune Of Fire");
        books[15] = new Book("Annihilation Of The Nation");
        books[16] = new Book("Separated In The Slaves");
        books[17] = new Book("Remember The Commander");
        books[18] = new Book("Thief Of Hope");
        books[19] = new Book("Swindler Without Faith");
        books[20] = new Book("Foes Of The Lost Ones");
        books[19] = new Book("1100");
        books[20] = new Book("1000");

        return books;
    }
}
