package com.kenfogel.binarytree.gui;

import com.kenfogel.binarytree.implementation.BinaryTree;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * A BinaryTree that can draw itself on a canvas
 * Draws itself using the paths from the binary tree it contains
 *
 * @param <T> The type that the tree holds
 */
public class GraphicBinaryTree<T> {
    private Canvas canvas;
    private BinaryTree<T> binaryTree;
    private GraphicsContext gc;

    // Initially the coordinate units map 1:1 to the pixels in the canvas
    // The multiplier lets us control the scale of the coordinate units
    private int multiplier = 5;

    // Defines the diameter of the circles
    private int circleDiameter = 8;

    // Defines the max number of characters in the circles
    private int maxTextLength = 6;


    public GraphicBinaryTree(Canvas canvas, BinaryTree<T> binaryTree) {
        this.canvas = canvas;
        this.binaryTree = binaryTree;
        this.gc = canvas.getGraphicsContext2D();
    }

    /**
     * Draws the tree on the canvas
     * Uses the paths that are returned from getPaths
     */
    public void draw() {
        T[][] paths = binaryTree.getPaths();

        // Finding the max path length to determine how big the canvas needs to be
        int maxPathLength = 0;
        for (T[] path: paths) {
            if (path.length > maxPathLength)
                maxPathLength = path.length;
        }

        // If maxPathLength is 0, we exit the method
        if (maxPathLength == 0) {
            return;
        }

        setupCanvas(maxPathLength);

        // All paths will be drawn from the root coordinates
        double rootY = 0;
        double rootX = canvas.getWidth() / 2;

        // Drawing each path
        for (int i = 0; i < paths.length; i++) {
            drawPath(paths[i], maxPathLength, rootX, rootY);
        }
    }

    /**
     * Helper method to draw a path in the canvas
     */
    private void drawPath(T[] path, int maxPathLength, double rootX, double rootY) {
        // Initially the line length will be the longest line length
        int currentLineLength = (int) Math.pow(2, maxPathLength);

        double currentX = rootX;
        double currentY = rootY;

        double ovalWidth = circleDiameter * multiplier;
        double ovalHeight = circleDiameter * multiplier;

        // To position correctly the ovals and text we need to play slightly with the coordinates
        gc.strokeOval(currentX - (ovalWidth / 2.5), currentY, ovalWidth, ovalHeight);
        gc.fillText(ellipsize(path[0].toString()), currentX - (ovalWidth / 3), currentY + (ovalHeight / 2));

        // Moving the currentY down by the circleDiameter
        currentY = currentY + (circleDiameter * multiplier);

        for (int i = 1; i < path.length; i++) {
            boolean isLeftNode = true;
            if (binaryTree.getComparator().compare(path[i], path[i - 1]) > 0)
                isLeftNode = false;

            double startX = currentX;
            double startY = currentY;

            for (int j = 0; j < currentLineLength; j++) {
                if (isLeftNode) {
                    currentY = currentY + (1 * multiplier);
                    currentX = currentX - (1 * multiplier);
                } else {
                    currentY = currentY + (1 * multiplier);
                    currentX = currentX + (1 * multiplier);
                }
            }

            double endX = currentX;
            double endY = currentY;

            gc.strokeLine(startX, startY, endX, endY);

            // Once again, to position correctly the ovals and text we need to play with the coordinates
            gc.strokeOval(currentX - (ovalWidth / 2), currentY, ovalWidth, ovalHeight);
            gc.fillText(ellipsize(path[i].toString()), currentX - (ovalWidth / 2.5), currentY + (ovalHeight / 2));

            currentY = currentY + (circleDiameter * multiplier);

            // The line length gets smaller after every level (so they will be spaced correctly)
            currentLineLength /= 2;
        }
    }

    /**
     * Helper method to prepare the canvas for drawing
     *
     * The topmost lines that link the nodes need to be long.
     * On the next levels, the lines need to be shorter.
     * That way, all the node values are always spaced apart.
     *
     * The bottom-most line starts with a length of 4 units (2^2) (so that there will enough space
     * between the most bottom nodes)
     * The lines in the upper levels have their lengths incremented in powers of 2 (2^3, 2^4, 2^5 ...)
     */
    private void setupCanvas(int maxPathLength) {
        // Initializing the height with the first circleDiameter so it will be easier to calculate the height
        // (it represents the bottom most circle that holds the node values)
        // We exit from the method if the maxPathLength is 0, so it is safe to assume
        // that there will be at least one node
        int canvasHeight = circleDiameter;
        for (int i = 1; i < maxPathLength; i++) {
            // i + 1 because we need to start adding the powers starting from 2^2 units and not 2^1 units
            canvasHeight += Math.pow(2, i + 1) + circleDiameter;
        }

        int canvasWidth = 0;
        // Calculating half of the width
        for (int i = 1; i < maxPathLength; i++) {
            canvasWidth += Math.pow(2, i + 1);
        }

        // Adding the other half of the width.
        // (we also need to add space for at least one circle so that nothing will be out of the screen)
        canvasWidth = (canvasWidth * 2) + circleDiameter;

        // Applying multiplier so that the tree will be bigger/smaller if needed
        canvasHeight *= multiplier;
        canvasWidth *= multiplier;

        canvas.setHeight(canvasHeight);
        canvas.setWidth(canvasWidth);
    }

    /**
     * Helper method to ellipsize the text if it is too long
     */
    private String ellipsize(String text) {
        if (text.length() < maxTextLength) {
            return text;
        } else {
            String newText = text.substring(0, maxTextLength - 2);
            newText += "..";
            return newText;
        }
    }
}
