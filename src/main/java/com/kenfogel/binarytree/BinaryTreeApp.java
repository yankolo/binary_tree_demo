package com.kenfogel.binarytree;

import com.kenfogel.binarytree.gui.BinaryTreeController;
import com.kenfogel.binarytree.implementation.BinaryTree;
import com.kenfogel.binarytree.data.Book;
import com.kenfogel.binarytree.comparators.BookComparator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Comparator;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 */
public class BinaryTreeApp extends Application {
    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/gui/BinaryTree.fxml"));
        Parent window = loader.load();
        BinaryTreeController binaryTreeController = loader.getController();

        Scene scene = new Scene(window);
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();
    }
}
